/*
 * This file is part of dscomp-binary.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <dscomp-module.h>

#include "binary.h"

static void *
new_func ( void )
{
	binary_t * const binary = binary_new();
	return binary;
}

static void __attribute__((nonnull))
destroy_func ( void * const binary )
{
	binary_destroy(binary);
	return;
}

static int __attribute__((nonnull))
insert_func ( void * const binary, const int key, const int value )
{
	const binary_error_t ret = binary_insert(binary, key, value);
	if ( ret == BINARY_OK ) {
		return DSCOMP_OK;
	} else if ( ret == BINARY_EXISTS ) {
		return DSCOMP_EXITSTS;
	} else {
		return DSCOMP_ERROR;
	}
}

static int __attribute__((nonnull))
search_func ( void * const binary, const int key, int * const valuep )
{
	if ( binary_search(binary, key, valuep) ) {
		return DSCOMP_NOT_FOUND;
	}
	return DSCOMP_OK;
}

static int __attribute__((nonnull))
clear_func ( void * const binary )
{
	binary_clear(binary);
	return DSCOMP_OK;
}

static int __attribute__((nonnull))
check_func ( void * const binary )
{
	if ( binary_check(binary) ) {
		return DSCOMP_ERROR;
	}
	return DSCOMP_OK;
}

#if RECYCLE
# define module binary_recycle_LTX_module
#else /* !RECYCLE */
# define module_binary_LTX_module
#endif /* !RECYCLE */

const dscomp_module_t module = {
	&new_func,
	&destroy_func,
	&insert_func,
	&search_func,
	&clear_func,
	&check_func
};
