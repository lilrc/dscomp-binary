/*
 * This file is part of dscomp-binary.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "binary.h"

/* true */
#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif /* HAVE_STDBOOL_H */

/* free(), malloc() */
#include <stdlib.h>

/* perror() */
#include <stdio.h>

#if RECYCLE
# define binary_node_remove(binary, node) \
	binary_node_recycle(binary, node)
#else /* !RECYCLE */
# define binary_node_remove(binary, node) binary_node_destroy(node)
#endif /* !RECYCLE */

static inline void __attribute__((nonnull))
binary_node_init ( binary_node_t * const node,
                   const int key,
                   const int value )
{
	node->key = key;
	node->value = value;
	node->left = NULL;
	node->right = NULL;
	return;
}

static inline binary_node_t * __attribute__((nonnull))
binary_node_new ( binary_t * const binary,
                  const int key,
                  const int value )
{
#if RECYCLE
	if ( binary->recycled != NULL ) {
		binary_node_t * const node = binary->recycled;
		binary->recycled = node->right;
		binary_node_init(node, key, value);
		return node;
	}
#endif /* RECYCLE */

	binary_node_t * const node = malloc(sizeof(binary_node_t));
	if ( node == NULL ) {
		perror("malloc");
		return NULL;
	}
	binary_node_init(node, key, value);
	return node;
}

static void __attribute__((nonnull))
binary_node_destroy ( binary_node_t * const node )
{
	free(node);
	return;
}

#if RECYCLE
static void __attribute__((nonnull))
binary_node_recycle ( binary_t * const binary,
                      binary_node_t * const node )
{
	node->right = binary->recycled;
	binary->recycled = node;
	return;
}
#endif /* RECYCLE */

static void __attribute__((nonnull))
binary_node_remove_recursive ( binary_t * const binary,
                               binary_node_t * const node )
{
	if ( node->left != NULL ) {
		binary_node_remove_recursive(binary, node->left);
	}
	if ( node->right != NULL ) {
		binary_node_remove_recursive(binary, node->right);
	}
	binary_node_remove(binary, node);
	return;
}

binary_t *
binary_new ( void )
{
	binary_t * const binary = malloc(sizeof(binary_t));
	if ( binary == NULL ) {
		perror("malloc");
		return NULL;
	}

	binary->root = NULL;

#if RECYCLE
	binary->recycled = NULL;
#endif /* RECYCLE */

	return binary;
}

void __attribute__((nonnull))
binary_destroy ( binary_t * const binary )
{
	binary_clear(binary);

#if RECYCLE
	binary_node_t * node = binary->recycled;
	while ( node != NULL ) {
		binary_node_t * const next = node->right;
		binary_node_destroy(node);
		node = next;
	}
#endif /* RECYCLE */

	free(binary);
	return;
}

void __attribute__((nonnull))
binary_clear ( binary_t * const binary )
{
	if ( binary->root != NULL ) {
		binary_node_remove_recursive(binary, binary->root);
		binary->root = NULL;
	}
	return;
}

binary_error_t __attribute__((nonnull))
binary_insert ( binary_t * const binary,
                const int key,
                const int value )
{
	binary_node_t * const node = binary_node_new(binary, key, value);
	if ( node == NULL ) {
		return BINARY_NOMEM;
	}

	if ( binary->root != NULL ) {
		binary_node_t * parent = binary->root;
		while ( true ) {
			if ( key > parent->key ) {
				if ( parent->right != NULL ) {
					parent = parent->right;
				} else /* if ( parent->right == NULL ) */ {
					parent->right = node;
					return BINARY_OK;
				}
			} else if ( key < parent->key ) {
				if ( parent->left != NULL ) {
					parent = parent->left;
				} else /* if ( parent->left == NULL ) */ {
					parent->left = node;
					return BINARY_OK;
				}
			} else /* if ( key == parent->key ) */ {
				binary_node_remove(binary, node);
				return BINARY_EXISTS;
			}
		}
	} else {
		binary->root = node;
		return BINARY_OK;
	}
}

int __attribute__((nonnull))
binary_search ( const binary_t * const binary,
                const int key,
                int * const valuep )
{
	const binary_node_t * node = binary->root;
	while ( node != NULL ) {
		if ( key > node->key ) {
			node = node->right;
		} else if ( key < node->key ) {
			node = node->left;
		} else /* if ( key == node->key ) */ {
			*valuep = node->value;
			return 0;
		}
	}
	return 1;
}

static int __attribute__((nonnull))
binary_node_check_recursive ( const binary_node_t * const node )
{
	if ( node->left != NULL ) {
		const binary_node_t * const left = node->left;
		if ( node->key <= left->key ) {
			return 1;
		}
		binary_node_check_recursive(left);
	} 
	if ( node->right != NULL ) {
		const binary_node_t * const right = node->right;
		if ( node->key >= right->key ) {
			return 1;
		}
		binary_node_check_recursive(right);
	}
	return 0;
}

int __attribute__((nonnull))
binary_check ( const binary_t * const binary )
{
	if ( binary->root != NULL ) {
		return binary_node_check_recursive(binary->root);
	} else {
		return 0;
	}
}
