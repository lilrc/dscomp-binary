/*
 * This file is part of dscomp-binary.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef BINARY_H
# define BINARY_H

/* Define to recycle destroyed nodes to avoid unnecessary calls to
 * malloc(). */
# if !defined(RECYCLE)
#  define RECYCLE 0
# endif /* !defined(RECYCLE) */

enum binary_error_e {
	BINARY_OK,
	BINARY_EXISTS,
	BINARY_NOMEM
};
typedef enum binary_error_e binary_error_t;

struct binary_node_s {
	int key;
	int value;
	struct binary_node_s * left;
	struct binary_node_s * right;
};
typedef struct binary_node_s binary_node_t;

struct binary_s {
	binary_node_t * root;
# if RECYCLE
	binary_node_t * recycled;
# endif /* RECYCLE */
};
typedef struct binary_s binary_t;

binary_t * binary_new ( void );
void binary_destroy ( binary_t * const binary )
	__attribute__((nonnull));

void binary_clear ( binary_t * const binary )
	__attribute__((nonnull));

binary_error_t binary_insert ( binary_t * const binary,
                               const int key,
                               const int value )
	__attribute__((nonnull));

int binary_search ( const binary_t * const binary,
                    const int key,
                    int * const valuep )
	__attribute__((nonnull));

int binary_check ( const binary_t * const binary )
	__attribute__((nonnull));

#endif /* !BINARY_H */
